#!/bin/sh
NODB=$1
BASE=/var/www/images
PLOTGAS=gashourly.png
OUT=/tmp/$$.out
HOUR=$(date +%H)
DATE=$(date "+%F %T")
HOURAGO=$(date +%H --date="hour ago")
NEXTHOUR=$(date +%H --date="next hour")
MYSQL="mysql -s -s -s -usmartmeter -psmartmeter -hmariadb smartmeter"
SMHOST=smartmeter_host
SMPORT=50000

#time:to1:to2:by1:by1:act1:act2:gas
LINE=$(nc -w 10 --recv-only $SMHOST $SMPORT)
if echo $LINE |grep -q ^Ncat  ; then
	echo $LINE
	exit 2
fi

if echo $LINE |grep -q ^Unable  ; then
        echo $LINE
        exit 2
fi

M3=$(echo $LINE|awk -F: '{print $8}')


LAST=$(echo "SELECT lastm3 from hourlygasm3 where hourly='$HOURAGO'" | $MYSQL)
if (( $(echo "$LAST > 0" |bc -l) )); then
	CURRENT=$(echo "scale=3;($M3 - $LAST)" | bc -l)
else
	CURRENT=0
fi
if [ ! $NODB ]; then
	echo "UPDATE hourlygasm3 set hourlym3='$CURRENT',lastm3='$M3' where hourly='$HOUR';" | $MYSQL
	echo "UPDATE hourlygasm3 set hourlym3='0',lastm3='0' where hourly='$NEXTHOUR';" | $MYSQL
fi

echo "SELECT hourly,hourlym3 from hourlygasm3" | $MYSQL >$OUT

YRANGE=$(echo "SELECT max(hourlym3) from hourlygasm3" | $MYSQL);
YRANGE=$(echo "scale=0;($YRANGE + .1)" | bc -l)

gnuplot<<EOF
set term png medium size 500,200
set grid ytic
set grid xtic
set boxwidth 0.75
set timefmt "%H"
set ylabel "gas m3" 
set yrange [0:$YRANGE]
set xrange [00:23]
set output "$BASE/$PLOTGAS"
set style fill transparent solid 0.5 noborder
plot "$OUT"  using 2:xtic(1) with boxes lc rgb "green" title "$DATE: $CURRENT",  '' using 0:2:2 with labels notitle
set output
set term pop
EOF
#set xdata time
#plot "$OUT" using 1:2 with boxes lc rgb "green"  title "Hourly 24H"

rm $OUT
