#!/bin/sh
NODB=$1
BASE=/home/huis/www/public/privimages
PLOTKWH=kwhhourly.png
OUT=/tmp/$$.out
HOUR=$(date +%H)
DATE=$(date "+%F %T")
HOURAGO=$(date +%H --date="hour ago")
NEXTHOUR=$(date +%H --date="next hour")
MYSQL="mysql -s -s -s -usmartmeter -psmartmeter -hmariadb smartmeter"
SMHOST=smartmeter_host
SMPORT=50000

#time:to1:to2:by1:by1:act1:act2:gas
LINE=$(nc -w 10 --recv-only $SMHOST $SMPORT)
if echo $LINE |grep -q ^Ncat  ; then
	echo $LINE
	exit 2
fi

if echo $LINE |grep -q ^Unable  ; then
        echo $LINE
        exit 2
fi

KWH1=$(echo $LINE|awk -F: '{print $2}')
KWH2=$(echo $LINE|awk -F: '{print $3}')


LAST1=$(echo "SELECT last1 from hourlykwh where hourly='$HOURAGO'" | $MYSQL)
LAST2=$(echo "SELECT last2 from hourlykwh where hourly='$HOURAGO'" | $MYSQL)
if (( $(echo "$LAST1 > 0" |bc -l) )); then
	CURRENT1=$(echo "scale=3;($KWH1 - $LAST1)" | bc -l)
else
	CURRENT1=0
fi
if (( $(echo "$LAST2 > 0" |bc -l) )); then
	CURRENT2=$(echo "scale=3;($KWH2 - $LAST2)" | bc -l)
else
	CURRENT2=0
fi

if (( $(echo "$CURRENT1 > 0" |bc -l) )); then
        CURRENT=$CURRENT1
else
        CURRENT=$CURRENT2
fi

if [ ! $NODB ]; then
	echo "UPDATE hourlykwh set hourlykwh='$CURRENT',last1='$KWH1',last2='$KWH2' where hourly='$HOUR';" | $MYSQL
	echo "UPDATE hourlykwh set hourlykwh='0',last1='0',last2='0' where hourly='$NEXTHOUR';" | $MYSQL
fi


echo "SELECT hourly,hourlykwh from hourlykwh" | $MYSQL >$OUT

YRANGE=$(echo "SELECT max(hourlykwh) from hourlykwh" | $MYSQL);
YRANGE=$(echo "scale=0;($YRANGE + 1)" | bc -l)


gnuplot<<EOF
set term png medium size 500,200
set grid ytic
set grid xtic
set boxwidth 0.75
set timefmt "%H"
set ylabel "kwh" 
set yrange [0:$YRANGE]
set xrange [00:23]
set output "$BASE/$PLOTKWH"
set style fill transparent solid 0.5 noborder
plot "$OUT"  using 2:xtic(1) with boxes lc rgb "red" title "$DATE: $CURRENT",  '' using 0:2:2 with labels notitle
set output
set term pop
EOF
#set xdata time
#plot "$OUT" using 1:2 with boxes lc rgb "green"  title "Hourly 24H"

rm $OUT
