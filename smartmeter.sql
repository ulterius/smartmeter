--
-- Table structure for table `bydailykwh`
--

DROP TABLE IF EXISTS `bydailykwh`;
CREATE TABLE `bydailykwh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `kwh1` float DEFAULT NULL,
  `kwh2` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `bykwh`
--

DROP TABLE IF EXISTS `bykwh`;
CREATE TABLE `bykwh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `kwh1` float DEFAULT NULL,
  `kwh2` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `dailygasm3`
--

DROP TABLE IF EXISTS `dailygasm3`;
CREATE TABLE `dailygasm3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `m3` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `dailykwh`
--

DROP TABLE IF EXISTS `dailykwh`;
CREATE TABLE `dailykwh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `kwh1` float DEFAULT NULL,
  `kwh2` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `gasm3`
--

DROP TABLE IF EXISTS `gasm3`;
CREATE TABLE `gasm3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `m3` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `hourlygasm3`
--

DROP TABLE IF EXISTS `hourlygasm3`;
CREATE TABLE `hourlygasm3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hourly` varchar(10) DEFAULT NULL,
  `hourlym3` float DEFAULT NULL,
  `lastm3` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `hourlykwh`
--

DROP TABLE IF EXISTS `hourlykwh`;
CREATE TABLE `hourlykwh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hourly` varchar(10) DEFAULT NULL,
  `hourlykwh` float DEFAULT NULL,
  `last1` float DEFAULT NULL,
  `last2` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--
-- Table structure for table `kwh`
--

DROP TABLE IF EXISTS `kwh`;
CREATE TABLE `kwh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `kwh1` float DEFAULT NULL,
  `kwh2` float DEFAULT NULL,
  PRIMARY KEY (`id`)
)
