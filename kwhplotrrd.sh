#!/bin/bash
#RRD via xinetd
#/etc/xinetd.d/rrdsrv
# default: on
# description: RRDTool as a service
#service rrdsrv
#{
#        disable         = no
#        socket_type     = stream
#        protocol        = tcp
#        wait            = no
#        user            = root
#        server          = /usr/bin/rrdtool
#        server_args     = -
#         log_type        = FILE /var/log/rrdsrv.log
#}

RRDHOST=rrdhost
RRDPORT=13900
SMHOST=smartmeterhost
SMPORT=50000
RRDDB=/var/rrd/smartmeterkwh.rrd
RRDPLOT=/var/www/images/smartmeterkwh.png

nc $RRDHOST $RRDPORT <<EOF>/tmp/out.$$
info $RRDDB 
EOF

if grep -q ^ERROR /tmp/out.$$ ; then
nc $RRDHOST $RRDPORT <<EOF
create $RRDDB --step 60 --start N DS:kwh:GAUGE:300:U:U RRA:AVERAGE:0.5:1:525600 RRA:MIN:0.5:12:525600 RRA:MAX:0.5:12:525600 RRA:LAST:0.5:12:525600
EOF
fi

LINE=$(nc -w 10--recv-only $SMHOST $SMPORT)
if echo $LINE |grep -q ^Ncat  ; then
        echo $LINE
        exit 2
fi

if echo $LINE |grep -q ^Unable  ; then
        echo $LINE
        exit 2
fi

echo $LINE
KWH1=$(echo $LINE|awk -F: '{print $6}')
KWH2=$(echo $LINE|awk -F: '{print $7}')
if (( $(echo "$KWH1 > 0" |bc -l) )); then
	W=$KWH1
else
	W=$KWH2
fi

nc $RRDHOST $RRDPORT <<EOF
update $RRDDB N:${W}
EOF


nc $RRDHOST $RRDPORT <<EOF
graph --color GRID#006600 --color MGRID#005000 $RRDPLOT -E -a PNG -w 300 -h 80  -t KWH "DEF:kwh=$RRDDB:kwh:MAX" 'LINE1:kwh#ffae00:KWH' 'GPRINT:kwh:LAST:\:%2.0lf'  'GPRINT:kwh:MIN:Min\: %2.0lf' 'GPRINT:kwh:MAX:Max#\: %2.0lf'  'GPRINT:kwh:AVERAGE:Gem\: %2.0lf'
EOF
rm /tmp/out.$$
