# Smartmeter

## Name
Smartmeter, eg Slimme meter

## Description
Yet another project to read your smartmeter at your home with a serial cable. Like the landis gyr e350/e360.
I'm using a small perl script, with xinetd. Which can be used as a rpc call.
A bash script to create the graphs and php script to visualize.
The data is being saved in a mysql database.


## Installation
I'm using a rasberry pi with the special serial cable, usb <-> rj12.
- Create a mysql database and read in de smartmeter.sql file.
- Copy the perl script readmeter.pl to  the rasberry into /usr/local/bin and copy  xinetd_smartmeter to /etc/xinetd.d. Add the service with the port to /etc/services. Restart xinetd
- Check the serial line config in /usr/local/bin/readmeter.pl.  Running the script wil output a singe line with the values.  Otherwise check the output with minicom forexample.
- telnet localhost smartmeter will output the same.
- Use  plotkwh.sh to create the graphs. It uses gnuplot. Put in  crontab just before midnight.
- plotgashourly.sh plotkwhhourly.sh for the hourly graphs, use the fill scripts to initialise the tables with the hours.

persistent name for the tty device, forexample ttyUSB0:
udevadm info -a -n /dev/ttyUSB0
or
dmesg

Make a file with forexample: etc/udev/rules.d/90-persistent-smartmeter.rules

#New USB device found, idVendor=0403, idProduct=6001, bcdDevice= 6.00
#[    2.090860] usb 1-1.4: New USB device strings: Mfr=1, Product=2, SerialNumber=3

SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", SYMLINK+="ttysmartmeter"

this will create a link for the device





