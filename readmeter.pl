#!/usr/bin/perl

use Device::SerialPort;
use Time::Local;
$dev='/dev/ttyUSB0';
$lockdev="/usr/sbin/lockdev";

$err=system "$lockdev $dev";
while($err){
	$err=system "$lockdev $dev";
	sleep(1);
}
system "$lockdev -l $dev";
my $port = Device::SerialPort->new($dev) or die "Unable to open $dev";
$port->databits(8);
$port->baudrate(115200);
$port->parity("none");
$port->stopbits(1);
$in='';
$out='';
while(1) {
	my $byte=$port->read(1);
	#print "$byte";
	$in.=$byte;
	if($byte eq  "\n"){
		#Look for beginning
		if ($in=~/1-3:0.2.8/){
			$go=TRUE;
		}
		#Date/time return epoch
		if ($in=~/0-0:1.0.0/ && $go){
			$out.=datetime($in).":";
		}
		#Meter Reading electricity delivered to client (Tariff 1) in 0,001 kWh
		if ($in=~/1-0:1.8.1/ && $go){
			$out.=kwh($in).":";
		}
		#Meter Reading electricity delivered to client (Tariff 2) in 0,001 kWh
		if ($in=~/1-0:1.8.2/ && $go){
			$out.=kwh($in).":";
		}
		#Meter Reading electricity delivered BY client (Tariff 1) in 0,001 kWh
		if ($in=~/1-0:2.8.1/ && $go){
			$out.=kwh($in).":";
		}
		#Meter Reading electricity delivered BY client (Tariff 2) in 0,001 kWh
		if ($in=~/1-0:2.8.2/ && $go){
			$out.=kwh($in).":";
		}
		#Actual electricity power delivered in 1 Watt resolution
		if ($in=~/1-0:1.7.0/ && $go){
			$out.=kwh($in).":";
		}
		#Actual electricity power delivered in 1 Watt resolution
		if ($in=~/1-0:2.7.0/ && $go){
			$out.=kwh($in).":";
		}
		#Last 5-minute value (temperature converted), gas delivered to client in m3
		if ($in=~/0-1:24.2.1/ && $go){
			$out.=m3($in).":";
		}
		#End
		if ($in=~/XMX5LGF0010458446060/ && $go){
			print $out;
			system "$lockdev -u $dev";
			exit;
		}
		$in="";
	}
}
sub datetime{
	$value=shift;
	$value=(split(/\(/,$value))[1];
	$value=(split(/\)/,$value))[0];
	$sec=substr($value,10,2);
	$min=substr($value,8,2);
	$hours=substr($value,6,2);
	$day=substr($value,4,2);
	$month=substr($value,2,2)-1;
	$year=substr($value,0,2);
	$time = timelocal($sec,$min,$hours,$day,$month,$year);
	return $time;
}
sub kwh{
	$value=shift;
	$value=(split(/\(/,$value))[1];
	$value=(split(/\*/,$value))[0];
	$value=~ s/^0+//;
	if($value == .000){
		$value=0;
	}
	return $value;
}
sub m3{
	$value=shift;
	$value=(split(/\(/,$value))[2];
	$value=(split(/\*/,$value))[0];
	$value=~ s/^0+//;
	return $value;
}

# 0-0:1.0.0	Date-time stamp of the P1 message
# 1-0:1.8.1	Meter Reading electricity delivered to client (Tariff 1) in 0,001 kWh
# 1-0:1.8.2	Meter Reading electricity delivered to client (Tariff 1) in 0,001 kWh
# 1-0:2.8.1	Meter Reading electricity delivered by client (Tariff 1) in 0,001 kWh
# 1-0:2.8.2	Meter Reading electricity delivered by client (Tariff 2) in 0,001 kWh
# 1-0:1.7.0	Actual electricity power delivered in 1 Watt resolution
# 1-0:2.7.0	Actual electricity power received in 1 Watt resolution
# 0-1:24.2.1	Last 5-minute value (temperature converted), gas delivered to client in m3

#1-3:0.2.8(50)
#0-0:1.0.0(211230153155W)
#0-0:96.1.1(4530303637303035383434363036303231)
#1-0:1.8.1(000234.736*kWh)
#1-0:1.8.2(000256.633*kWh)
#1-0:2.8.1(000000.000*kWh)
#1-0:2.8.2(000000.000*kWh)
#0-0:96.14.0(0002)
#1-0:1.7.0(00.744*kW)
#1-0:2.7.0(00.000*kW)
#0-0:96.7.21(00013)
#0-0:96.7.9(00005)
#1-0:99.97.0(1)(0-0:96.7.19)(000101010000W)(0000000377*s)
#1-0:32.32.0(00015)
#1-0:52.32.0(00015)
#1-0:72.32.0(00015)
#1-0:32.36.0(00000)
#1-0:52.36.0(00000)
#1-0:72.36.0(00000)
#0-0:96.13.0()
#1-0:32.7.0(232.1*V)
#1-0:52.7.0(231.0*V)
#1-0:72.7.0(232.4*V)
#1-0:31.7.0(001*A)
#1-0:51.7.0(001*A)
#1-0:71.7.0(001*A)
#1-0:21.7.0(00.294*kW)
#1-0:41.7.0(00.142*kW)
#1-0:61.7.0(00.308*kW)
#1-0:22.7.0(00.000*kW)
#1-0:42.7.0(00.000*kW)
#1-0:62.7.0(00.000*kW)
#0-1:24.1.0(003)
#0-1:96.1.0(4730303738353635353937353230373230)
#0-1:24.2.1(211230153003W)(00192.191*m3)
#!999E
#/XMX5LGF0010458446060



