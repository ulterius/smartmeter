#!/bin/sh
NODB=$1
BASE=/var/www/images
PLOTKWH=kwh.png
PLOTKWHBY=kwhby.png
PLOTM3=m3.png
OUT=/tmp/$$.out
TIME=$(date +%s)
MONTH=$((3600*24*30))
YEAR=$((3600*24*365))
MONTHAGO=$(($TIME-$MONTH))
SUM=0
TIME=$(date +%s)
DATE=$(date -I)
DATEYEARAGO=(date -I --date="year ago")
#YEARAGO=$(date +%s --date="year ago")
HOUR=$(date +%H)
MYSQL="mysql -s -s -s -usmartmeter -psmartmeter -hmariadb smartmeter"

SMHOST=smartmeter_host
SMPORT=50000

#time:to1:to2:by1:by1:act1:act2:gas
LINE=$(nc -w 10 --recv-only $SMHOST $SMPORT)
if echo $LINE |grep -q ^Ncat  ; then
	echo $LINE
	exit 2
fi

if echo $LINE |grep -q ^Unable  ; then
        echo $LINE
        exit 2
fi

TIME=$(echo $LINE|awk -F: '{print $1}')
KWH1=$(echo $LINE|awk -F: '{print $2}')
KWH2=$(echo $LINE|awk -F: '{print $3}')
BYKWH1=$(echo $LINE|awk -F: '{print $4}')
BYKWH2=$(echo $LINE|awk -F: '{print $5}')
M3=$(echo $LINE|awk -F: '{print $8}')

YEARAGO=$(($TIME-$YEAR))


LAST1=$(echo "SELECT kwh1 from dailykwh order by id DESC LIMIT 1" | $MYSQL)
LAST2=$(echo "SELECT kwh2 from dailykwh order by id DESC LIMIT 1" | $MYSQL)

CURRENT1=$(echo "scale=3;($KWH1 - $LAST1)" | bc -l)
CURRENT2=$(echo "scale=3;($KWH2 - $LAST2)" | bc -l)
if (( $(echo "$CURRENT1 < 0" |bc -l) )); then
	CURRENT1=0;
fi
if (( $(echo "$CURRENT2 < 0" |bc -l) )); then
	CURRENT2=0;
fi

if [ ! $NODB ]; then
	echo "INSERT INTO dailykwh (time, kwh1, kwh2) VALUES ($TIME, '$KWH1','$KWH2')" | $MYSQL
	echo "INSERT INTO kwh (time, kwh1, kwh2) VALUES ($TIME, '$CURRENT1', '$CURRENT2')" | $MYSQL
fi

echo "SELECT FROM_UNIXTIME(time,'%d-%m-%Y') as time,kwh1,kwh2 from kwh where time > $YEARAGO" | $MYSQL >$OUT

unset LAST1
unset LAST2
LAST1=$(tail -1 $OUT | awk '{print $2}')
LAST2=$(tail -1 $OUT | awk '{print $3}')
LASTDATE=$(tail -1 $OUT | awk '{print $1}')

YRANGE=$(echo "SELECT greatest(max(kwh1),max(kwh2)) from kwh where time > $YEARAGO" | $MYSQL);
YRANGE=$(echo "scale=0;($YRANGE + 1)" | bc -l)

gnuplot<<EOF
set term png medium size 800,300
set grid ytic
set grid xtic
set timefmt "%d-%m-%Y"
set xdata time
set ylabel "KWH"
set yrange [0:$YRANGE]
set xrange [$DATEYEARAGO:$DATE]
set output "$BASE/$PLOTKWH"
plot "$OUT" using 1:2 with boxes lc rgb "green"  title "Jaar overzicht: $LASTDATE $LAST1 KWH laag","$OUT" using 1:3 with boxes lc rgb "blue"  title "$LAST2 KWH hoog"
set output
set term pop
EOF




rm $OUT

#Delvered by, back
unset LAST1
unset LAST2
unset CURRENT1
unset CURRENT2
LAST1=$(echo "SELECT kwh1 from bydailykwh order by id DESC LIMIT 1" | $MYSQL)
LAST2=$(echo "SELECT kwh2 from bydailykwh order by id DESC LIMIT 1" | $MYSQL)
CURRENT1=$(echo "scale=3;($BYKWH1 - $LAST1)" | bc -l)
CURRENT2=$(echo "scale=3;($BYKWH2 - $LAST2)" | bc -l)
if (( $(echo "$CURRENT1 < 0" |bc -l) )) || [ ! $CURRENT1 ]; then
	CURRENT1=0;
fi
if (( $(echo "$CURRENT2 < 0" |bc -l) )) || [ ! $CURRENT2 ]; then
	CURRENT2=0;
fi

if [ ! $NODB ]; then
	echo "INSERT INTO bydailykwh (time, kwh1, kwh2) VALUES ($TIME, '$BYKWH1','$BYKWH2')" | $MYSQL
	echo "INSERT INTO bykwh (time, kwh1, kwh2) VALUES ($TIME, '$CURRENT1', '$CURRENT2')" | $MYSQL
fi

echo "SELECT FROM_UNIXTIME(time,'%d-%m-%Y') as time,kwh1,kwh2 from bykwh where time > $YEARAGO " | $MYSQL >$OUT

unset LAST1
unset LAST2
LAST1=$(tail -1 $OUT | awk '{print $2}')
LAST2=$(tail -1 $OUT | awk '{print $3}')
LASTDATE=$(tail -1 $OUT | awk '{print $1}')

YRANGE=$(echo "SELECT greatest(max(kwh1),max(kwh2)) from bykwh where time > $YEARAGO" | $MYSQL);
YRANGE=$(echo "scale=0;($YRANGE + 1)" | bc -l)

gnuplot<<EOF
set term png medium size 800,300
set grid ytic
set grid xtic
set timefmt "%d-%m-%Y"
set xdata time
set ylabel "Solar KWH"
set yrange [0:$YRANGE]
set xrange [$DATEYEARAGO:$DATE]
set output "$BASE/$PLOTKWHBY"
plot "$OUT" using 1:2 with boxes lc rgb "green"  title "Jaar overzicht: $LASTDATE $LAST1 KWH laag","$OUT" using 1:3 with boxes lc rgb "blue"  title "$LAST2 KWH hoog"
set output
set term pop
EOF

rm $OUT

#GAS

unset LAST1
unset CURRENT1
LAST1=$(echo "SELECT m3 from dailygasm3 order by id DESC LIMIT 1" | $MYSQL)
CURRENT1=$(echo "scale=3;($M3 - $LAST1)" | bc -l)

if (( $(echo "$CURRENT1 < 0" |bc -l) )); then
	CURRENT1=0;
fi

if [ ! $NODB ]; then
	echo "INSERT INTO dailygasm3 (time, m3) VALUES ($TIME, '$M3')" | $MYSQL
	echo "INSERT INTO gasm3 (time, m3) VALUES ($TIME, '$CURRENT1')" | $MYSQL
fi

echo "SELECT FROM_UNIXTIME(time,'%d-%m-%Y') as time,m3 from  gasm3 where time > $YEARAGO " | $MYSQL >$OUT

unset LAST1
LAST1=$(tail -1 $OUT | awk '{print $2}')
LASTDATE=$(tail -1 $OUT | awk '{print $1}')

YRANGE=$(echo "SELECT max(m3) from gasm3 where time  > $YEARAGO" | $MYSQL);
YRANGE=$(echo "scale=0;($YRANGE + 1)" | bc -l)

gnuplot<<EOF
set term png medium size 800,300
set grid ytic
set grid xtic
set timefmt "%d-%m-%Y"
set xdata time
set ylabel "Gas M3"
set yrange [0:$YRANGE]
set xrange [$DATEYEARAGO:$DATE]
set output "$BASE/$PLOTM3"
plot "$OUT" using 1:2 with boxes lc rgb "green"  title "Jaar overzicht: $LASTDATE $LAST1 GAS M3"
set output
set term pop
EOF

#rm $OUT
